﻿using System;
using Prg.ClientTest.AttackService;


// test 1-2-3
namespace Prg.ClientTest {
    class Program {
        static void Main(string[] args) {
            var max = (args.Length != 0 ? Convert.ToInt32(args[0]) : 5);
            var executionType = (args.Length != 0 ? Convert.ToInt32(args[1]) : 2);

            var client = new AttackService.ServiceClient();

            var start = DateTime.Now;

            if (executionType == 1) {
                client.GetDataCompleted += client_GetDataCompleted;
                var f = new Func<string, bool>((e) => {
                    for (var i = 0; i < max; i++) {
                        client.GetDataAsync(i + 1);
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("{0} - Request has been made! - {1}", e, DateTime.Now);
                    }
                    return true;
                });
                f("1");
            }
            else if (executionType == 2) {
                client.SendDataCompleted += client_SendDataCompleted;
                var connStr = System.Configuration.ConfigurationManager.AppSettings["ConnStr"];
                var f = new Func<string, bool>((e) => {
                    for (var i = 0; i < max; i++) {
                        client.SendDataAsync(Convert.ToString(i + 1), connStr);
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        Console.WriteLine("{0} - Request has been made! - {1}", e, DateTime.Now);
                    }
                    return true;
                });
                f("1");
            }

            Console.ForegroundColor = ConsoleColor.Green;
            //client.Close();
            Console.WriteLine("DONE - {0}-{1}", start, DateTime.Now);
            Console.ReadLine();
        }

        private static void client_SendDataCompleted(object sender, SendDataCompletedEventArgs e) {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Time : {0} - Saved to Database  ? :  {1}", DateTime.Now, e.Result.StringValue);
        }

        static void client_GetDataCompleted(object sender, AttackService.GetDataCompletedEventArgs e) {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Time : {0} - Value :  {1}", DateTime.Now, e.Result);
        }
    }
}

